## Values to provide:

variable "env" {
  type        = string
  description = "An environment code name that will be used in different names and paths"
}

variable "gitlab_vault_jwt_project_id" {
  type        = number
  description = "The gitlab project that will be granted vault access through JWT auth."
}

variable "jwt_bound_claims" {
  type        = map(any)
  description = <<EOT
  The pipeline conditions needed to gain vault access (ref protected, branch = main, etc.).
  The project ID condition will be auto-added using the 'gitlab_vault_jwt_project_id' variable.
  EOT
}

variable "project_name" {
  type        = string
  description = "Project name will be used as a prefix in all Vault resources (just like 'env')"
}

variable "read_secrets" {
  type = list(object({
    name        = string
    description = string
  }))
  default     = []
  description = <<EOT
  A list of (kv2) secrets that this module will auto generate READ policies for.
  Project name and ENV will be added to those secrets names.
  EOT
}

variable "transit_keys" {
  type = list(object({
    name             = string
    description      = string
    deletion_allowed = bool
  }))
  default     = []
  description = <<EOT
  A list of transit engine keys that this module will auto generate encrypt/decrypt policies for.
  Project name and ENV will be added to those key names.
  EOT
}

## Values to override if needed:

variable "backend_jwt" {
  type        = string
  default     = "gitlab"
  description = "The Vault mount point for JWT Auth backend"
}

variable "backend_transit" {
  type        = string
  default     = "transit"
  description = "The Vault mount point for the Transit backend"
}

variable "backend_kv" {
  type        = string
  default     = "kv"
  description = "The Vault mount point for KV(2) backend"
}
