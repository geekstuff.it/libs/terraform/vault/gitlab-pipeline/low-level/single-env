terraform {
  required_version = "~> 1.2"

  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.8"
    }
  }
}
