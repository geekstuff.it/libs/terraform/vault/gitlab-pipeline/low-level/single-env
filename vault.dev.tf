variable "enable_dev_approle" {
  type        = bool
  default     = false
  description = "Creates a dev purpose approle that will grant the same policies as gitlab-pipelines gets."
}

variable "backend_approle" {
  type        = string
  default     = "approle"
  description = "The Vault mount point for Approle backend"
}

resource "vault_approle_auth_backend_role" "dev" {
  count          = var.enable_dev_approle ? 1 : 0
  backend        = data.vault_auth_backend.auth_approle.path
  role_name      = "${var.project_name}_approledev_${var.env}"
  token_policies = []
}

resource "vault_approle_auth_backend_role_secret_id" "dev" {
  count     = var.enable_dev_approle ? 1 : 0
  backend   = data.vault_auth_backend.auth_approle.path
  role_name = vault_approle_auth_backend_role.dev[0].role_name
}

resource "vault_generic_secret" "dev_approle" {
  count = var.enable_dev_approle ? 1 : 0
  path  = "kv/${var.project_name}/${var.env}/dev_approle"
  data_json = jsonencode({
    backend   = data.vault_auth_backend.auth_approle.path
    role_id   = vault_approle_auth_backend_role.dev[0].role_id
    secret_id = vault_approle_auth_backend_role_secret_id.dev[0].secret_id
  })
}
