# lib \\ terraform \\ vault \\ gitlab-pipeline \\ low-level \\ single-env

This Terraform library helps setup a project's GitLab pipeline so it can fetch secrets using JWT auth and encrypt/decrypt plan file using transit engine.

All Vault resources created will use project and environment in their names.
